import os
import re
import sys
import channel
import xml.dom.minidom as minidom
import xml.sax
import threading
import argparse
import LocalTest
from datetime import datetime
from persistence import Persistence

debug_mode = True

"""
class TLS(threading.local):
    local = threading.local()

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __enter__(self):
        tls = self.local.__dict__
        tls.setdefault(self.name, []).append(self.value)

    def __exit__(self, exc_type, exc_val, exc_tb):
        tls = self.local.__dict__
        tls[self.name].pop(-1)
        if not tls[self.name]:
            del tls[self.name]

    @classmethod
    def get(cls, var, default=None):
        tls = cls.local.__dict__
        return default if var not in tls else tls[var][-1]
"""

def scene_tracker(func):
    if debug_mode:
        def _decorator(self, *args, **kwargs):
            # access a from TestSample
            variables = vars(self)
            short = {}
            for name, val in vars(self).items():
                if name not in ['parent', 'pos', 'type_id', 'program'] and len(str(val)) < 100:
                    short[name] = val
            script = self.get_parent(Script)
            if not script:
                script = self.get_parent(Scenario)
            print('%s:%d\t%s\t%s:%s\t%s' % (
                script.get_path(), self.pos[0], str(script.uid), self.__class__, func.__name__, str(short))
            )
            return func(self, *args, **kwargs)

        return _decorator
    else:
        return func


class Value:
    def __init__(self, dom, parent, **kwargs):
        self.id = None
        self.when = None
        self.parent = parent
        for name, val in kwargs.items():
            setattr(self, name, val)
        if dom:
            self.pos = dom.parse_position
            for name, val in dom.attributes.items():
                setattr(self, name, val)
        else:
            self.pos = (-1, -1)

    def enabled(self):
        if not self.when:
            return True
        parent = self.get_parent(Block)
        return parent.evaluate(self.when)

    def to_str(self):
        if not self.enabled():
            return ''
        parent = self.get_parent(Block)
        env = parent.get_env()
        return str(eval(self.of, env))

    def get_parent(self, cls):
        if not self.parent:
            return None

        if isinstance(self.parent, cls):
            return self.parent
        return self.parent.get_parent(cls)


class Set(Value):
    def __init__(self, dom, parent, **kwargs):
        Value.__init__(self, dom, parent, **kwargs)
        self.text = []
        for child in dom.childNodes:
            if child.nodeType in [child.TEXT_NODE, child.CDATA_SECTION_NODE]:
                if child.nodeValue.strip():
                    self.text.append(child.nodeValue)

    @scene_tracker
    def do(self):
        parent = self.get_parent(Block)
        text = '\n'.join(self.text)
        val = parent.evaluate(text) if text.strip() else None
        parent.set_var(self.var, val)


class Str(Value):
    def __init__(self, dom, parent, **kwargs):
        Value.__init__(self, dom, parent, **kwargs)
        self.text = []

        if dom:
            if type(self) in [Str, Msg, Ask]:
                for child in dom.childNodes:
                    if child.nodeType == child.TEXT_NODE:
                        if child.nodeValue.strip():
                            self.text.append(child.nodeValue)
                    else:
                        assert (child.tagName == 'value')
                        cls = globals()[child.tagName.title()]
                        obj = cls(child, self)
                        self.text.append(obj)

    def to_str(self):
        if not self.enabled():
            return ''

        def extract(obj):
            if type(obj) is Value:
                return obj.to_str()
            else:
                return obj

        res = [extract(item) for item in self.text]
        return ''.join(res)


def resolve(self):
    if self.str:
        strings = self.get_parent(Block).get('str', self.str, [])
        if strings:
            return [item.to_str() for item in strings if item.enabled()] or [self.str]
        else:
            return [self.str]
    else:
        return [self.to_str()]


class Msg(Str):
    def __init__(self, dom, parent, **kwargs):
        self.str = None
        self.media = None
        self.to = None
        Str.__init__(self, dom, parent, **kwargs)

    @scene_tracker
    def do(self):
        if not self.enabled():
            return
        env = self.get_parent(Block).get_env()
        text = ''.join(resolve(self))

        def arg(text):
            return eval(text[3:], env) if text and text.startswith('?py') else text

        self.get_parent(Scenario).dispatch_message(text, arg(self.to), arg(self.media))


class Enable:
    def __init__(self, dom, parent, **kwargs):
        for name, val in dom.attributes.items():
            setattr(self, name, val)

        new_args = {}
        new_args.update(kwargs)
        new_args['when'] = self.when

        for child in dom.childNodes:
            if child.nodeType in [child.TEXT_NODE, child.COMMENT_NODE]:
                continue
            if child.PROCESSING_INSTRUCTION_NODE == child.nodeType:
                parent.add_child(child.nodeName, ProcessingInstruction(child, parent))
            else:
                cls = globals()[child.tagName.title()]
                obj = cls(child, parent, **new_args)
                parent.add_child(child.tagName, obj)


class Ask(Msg):
    def __init__(self, dom, parent, **kwargs):
        self.validator = None
        Msg.__init__(self, dom, parent, **kwargs)

    @scene_tracker
    def do(self):
        if not self.enabled():
            return
        while True:
            Msg.do(self)
            text = self.get_parent(Scenario).wait_input()
            parent = self.get_parent(Block)
            parent.set_var(self.var, text)
            if not self.validator or (
            self.regexp(text, self.validator) if self.validator[0] == '/' else eval(self.validator, parent.get_env())):
                break
        self.get_parent(Scenario).get_api().chrono.on_user_input()

    @classmethod
    def regexp(cls, text, regexp):
        pos = regexp.rfind('/')
        pattern = re.compile(regexp[1:pos], re.IGNORECASE if 'i' in regexp[pos + 1:] else 0)
        return pattern.match(text)


class Input(Str):
    def __init__(self, dom, parent, **kwargs):
        Str.__init__(self, dom, parent, **kwargs)
        self.btns = []

    @scene_tracker
    def do(self):
        if not self.enabled():
            return
        options = {}
        for btn in self.btns:
            if btn.get_for():
                res = self.get_parent(Block).evaluate(btn.get_for())
                for val in res:
                    clone = btn.clone()
                    if clone.arg:
                        clone.set_var(clone.arg, val)
                    options[val] = clone
            else:
                options[btn.get_text()] = btn
        env = self.get_parent(Block).get_env()
        scenario = self.get_parent(Scenario)
        while True:
            scenario.send_buttons('>>', list(options.keys()))
            text = scenario.wait_input()
            if text in options:
                self.get_parent(Scenario).get_api().chrono.on_user_input()
                options[text].do()
                break

    def add_btn(self, btn):
        self.btns.append(btn)


class ProcessingInstruction:
    def __init__(self, dom, parent):
        self.target = dom.target
        self.parent = parent
        self.code = dom.nodeValue.strip()

    def do(self):
        if 'py' == self.target:
            self.parent.evaluate(self.code)
        else:
            Block.call_block(self.parent, self.target, arg='{%s}' % self.code)


class Block(Value):
    def __init__(self, dom, parent, **kwargs):
        self.clone_kwargs = {'dom': dom, 'parent': parent}
        self.clone_kwargs.update(kwargs)
        self.arg = None
        setattr(self, 'for', None)
        Value.__init__(self, dom, parent, **kwargs)
        self.type_id = {}
        self.program = []
        self.scope = {}
        self.btn_input = None
        if not dom:
            return
        for child in dom.childNodes:
            if child.nodeType in [child.TEXT_NODE, child.COMMENT_NODE]:
                continue
            if child.PROCESSING_INSTRUCTION_NODE == child.nodeType:
                self.add_child(child.nodeName, ProcessingInstruction(child, self))
            else:
                cls = globals()[child.tagName.title()]
                obj = cls(child, self)
                if child.tagName not in ['enable']:
                    self.add_child(child.tagName, obj)

        if self.btn_input:
            self.program.append(self.btn_input)
            self.btn_input = None

    def add_child(self, type, child):
        if hasattr(child, 'id'):
            self.type_id.setdefault(type, {}).setdefault(child.id, []).append(child)

        if type == 'btn':
            if child.enabled():
                if not self.btn_input:
                    self.btn_input = Input(None, self)
                self.btn_input.add_btn(child)
        elif self.btn_input:
            self.program.append(self.btn_input)
            self.btn_input = None

        if type not in ['str', 'btn']:
            self.program.append(child)

    def get(self, type, id, default=None):
        if type in self.type_id and id in self.type_id[type]:
            return self.type_id[type][id]
        parent = self.get_parent(Block)
        return parent.get(type, id, default) if parent else default

    def get_env(self):
        parent = self.get_parent(Block)
        if parent:
            scope = parent.get_env()
            self.scope.update(scope)
            return self.scope
        else:
            return self.scope

    def evaluate(self, text):
        env = self.get_env()
        return eval(text, env)

    def set_var(self, name, val):
        if '/' in name:
            parts = name.split('/')
            parent = self
            for part in parts[1:]:
                parent = parent.get_parent(Block)
            parent.set_var(parts[-1], val)
        else:
            self.scope[name] = val
            self.propagate(name)

    @scene_tracker
    def do(self, filter=None):
        if not self.enabled():
            return
        if self.program:
            for step in self.program:
                if filter and not filter(step):
                    continue
                step.do()
        else:
            self.call_block(self, self.id, arg=self.arg, _for=self.get_for())

    def get_for(self):
        return super().__getattribute__('for')

    @classmethod
    def call_block(cls, self, id, arg=None, _for=None):
        scenario = self.get_parent(Script) or self.get_parent(Scenario)
        blocks = scenario.get_block().get('block', id, None)
        for block in blocks:
            clone = block.clone()
            clone.parent = self.parent
            if _for:
                env = self.get_env()
                items = eval(_for, env)
                for item in items:
                    clone.set_var(block.arg, item)
                    clone.do()
            else:
                if clone.arg:
                    env = self.get_env()
                    clone.set_var(block.arg, eval(arg, env))
                clone.do()

    def propagate(self, var):  # bubbles up the value
        assert (var in self.scope)
        parent = self
        parents = []
        while parent:
            parent = parent.get_parent(Block)
            if parent and var in parent.scope:
                parents.append(parent)
        if not parents:
            return
        for parent in parents:
            del parent.scope[var]
        parents[-1].scope[var] = self.scope[var]
        del self.scope[var]

    def clone(self):
        return Block(**self.clone_kwargs)

class Btn(Block):
    def __init__(self, dom, parent, **kwargs):
        Block.__init__(self, dom, parent, **kwargs)

    def get_text(self):
        return ''.join(resolve(self))

    @scene_tracker
    def do(self):
        if self.program:  # btn with no program is a dummy used for wrong answers
            Block.do(self)


class Timer(Block):
    def __init__(self, dom, parent, **kwargs):
        self.id = None
        Block.__init__(self, dom, parent, **kwargs)
        scenario = self.get_parent(Scenario)
        scenario.add_timer(self)
        self.last_tick = datetime.now()

    def get_text(self):
        return ''.join(resolve(self))

    @scene_tracker
    def do(self):
        Block.do(self)

    def tick(self):
        now = datetime.now()
        if (now - self.last_tick).total_seconds() < float(self.delay):
            return False
        self.last_tick = now
        return self.do()


class Script(Value):
    def __init__(self, dom, parent, **kwargs):
        self.id = None
        self.env = '{}'
        Value.__init__(self, dom, parent, **kwargs)
        self.block = None
        self.code = None
        self.uid = self.get_parent(Scenario).uid

    def get_path(self):
        return self.get_parent(Scenario).get_abs_path(self.src)

    def get_block(self):
        return self.block

    @scene_tracker
    def do(self):
        parent = self.get_parent(Block)
        if not self.block and not self.code:
            if self.src.endswith('.xml'):
                env = parent.get_env()
                env.update(eval(self.env))
                dom = self.get_parent(Scenario).load_script(self.src)
                self.block = Block(dom.documentElement, self)
                for var, val in env.items():
                    self.block.set_var(var, val)
            elif self.src.endswith('.py'):
                filename = self.get_path()
                with open(filename, "rb") as source_file:
                    self.code = compile(source_file.read(), filename, "exec")

        if self.code:
            env = parent.get_env()
            env.update(eval(self.env))
            exec(self.code, env)
        else:
            self.block.do(lambda block: block.id == self.start)

    @staticmethod
    def get_env():
        return {}

    @staticmethod
    def get(self, type, id, default=None):
        return None


class DuplexSession(threading.Thread):
    def __init__(self, channel, uid, dispatcher):
        threading.Thread.__init__(self)
        self.messages = []
        self.remote_messages = []
        self.channel = channel
        self.dispatcher = dispatcher
        self.uid = uid
        self.event = threading.Event()
        self.save = self.dispatcher.get_args().record

    def dispatch_message(self, text, to, media):
        if to:
            if 'self' == to:
                self.on_new_message(text)
            else:
                self.dispatcher.dispatch(to, text, "%s:%s" % (self.dispatcher.args.messenger, self.uid))
            return
        if media:
            if media.split('.', 1)[1] in ['jpg', 'jpeg', 'png']:
                self.channel.send_photo(self.uid, text, self.get_abs_path(media))
            elif media.split('.', 1)[1] in ['mp3']:
                self.channel.send_audio(self.uid, text, self.get_abs_path(media))
            elif 'youtube.com/watch?v=' in media:
                self.channel.send_video(self.uid, text, media)
            else:
                self.channel.send_video(self.uid, text, self.get_abs_path(media))
        else:
            self.channel.send_message(self.uid, text)
        self.messages = []

    def send_buttons(self, text, btns):
        self.messages = []
        self.channel.send_buttons(self.uid, text, btns)

    def on_new_message(self, text):
        self.messages.append(text)
        self.event.set()
        if self.save:
            self.record(text)

    def on_remote_message(self, text, sender):
        self.remote_messages.append((sender, text))

    def wait_input(self, text=''):
        if text:
            self.send_message(text)
        while not self.messages:
            self.tick_timers()
            self.event.wait(1)
        msg = self.messages.pop(0)
        if msg is None:
            raise StopIteration()
        elif '/start' == msg:
            raise EOFError()
        return msg

    def record(self, text):
        if text is None:
            return
        with open(os.path.join(self.save, 'session_%s.txt' % self.uid), 'a') as handle:
            handle.write(text + '\n')

    def replay(self, path):
        with open(path, 'r') as handle:
            for line in handle:
                self.on_new_message(line)


class MessagesAPI:
    def __init__(self, scenario):
        self.scenario = scenario.scenario

    def pop(self):
        if self.scenario.remote_messages:
            return self.scenario.remote_messages.pop(0)
        return None


class Chrono:
    def __init__(self, scenario):
        self.scenario = scenario.scenario
        self.last_user_input = datetime.now()

    @staticmethod
    def now():
        return datetime.now()

    def from_str(self, text):
        try:
            return datetime.strptime(text, '%Y-%m-%d %H:%M')
        except ValueError as err:
            self.scenario.send_message(str(err), None)
            return None

    def to_str(self, dt=datetime.now()):
        return dt.strftime('%Y-%m-%d %H:%M')

    def on_user_input(self):
        self.last_user_input = datetime.now()


class ScenarioAPI:
    def __init__(self, scenario):
        self.scenario = scenario
        self.messages = MessagesAPI(self)
        self.chrono = Chrono(self)

    def peers(self):
        return self.scenario.dispatcher.peers()


class Scenario(DuplexSession):
    def __init__(self, path, channel, uid, dispatcher):
        DuplexSession.__init__(self, channel, uid, dispatcher)
        self.path = path
        self.timers = []
        self.block = Block(self.parse(path).documentElement, self)
        api = ScenarioAPI(self)
        api.user = channel.get_user_data(uid)
        self.block.set_var('api', api)
        self.start()

    def get_api(self):
        return self.block.get_env()['api']

    def get_block(self):
        return self.block

    def get_path(self):
        return self.path

    @staticmethod
    def get_env():
        return {}

    @staticmethod
    def get(self, type, id, default=None):
        return self.block.get(type, id, default)

    @staticmethod
    def get_parent(cls):
        return None

    def get_abs_path(self, path):
        if not os.path.isabs(self.path):
            self.path = os.path.abspath(self.path)
        if not os.path.isabs(path):
            folder = os.path.dirname(self.path)
            path = os.path.join(folder, path)
        return path

    def load_script(self, path):
        return self.parse(self.get_abs_path(path))

    @staticmethod
    def parse(path):
        parser = xml.sax.make_parser()
        orig_set_content_handler = parser.setContentHandler

        def set_content_handler(dom_handler):
            def startElementNS(name, tagName, attrs):
                orig_start_cb(name, tagName, attrs)
                cur_elem = dom_handler.elementStack[-1]
                cur_elem.parse_position = (
                    parser._parser.CurrentLineNumber,
                    parser._parser.CurrentColumnNumber
                )

            orig_start_cb = dom_handler.startElementNS
            dom_handler.startElementNS = startElementNS
            orig_set_content_handler(dom_handler)

        parser.setContentHandler = set_content_handler
        return minidom.parse(path, parser)

    def do(self, vars={}, first='start'):
        for var, val in vars.items():
            self.block.set_var(var, val)

        while True:
            try:
                self.block.do(lambda block: block.id == first or isinstance(block, Script))
            except EOFError:
                self.restart()
            except StopIteration:
                return

    def run(self):
        self.do({'language': 'ru'})

    def restart(self):
        pass

    def add_timer(self, timer):
        self.timers.append(timer)

    def tick_timers(self):
        self.dispatcher.check_messages(self.uid)
        for timer in self.timers:
            timer.tick()


class MessageDispatcher(channel.Interface):
    def __init__(self, scenario, args):
        self.sessions = {}
        self.channel = None
        self.scenario = scenario
        self.args = args
        self.db = Persistence(os.path.join(os.path.dirname(self.scenario), 'db.xml'))

    def get_args(self):
        return self.args

    def set_channel(self, channel):
        self.channel = channel

    def send_message(self, uid, text, **kwargs):
        if uid not in self.sessions:
            session = Scenario(self.scenario, self.channel, uid, self)
            self.activate_session(uid, session)

        self.sessions[uid].on_new_message(text)

    def send_photo(self, uid, caption, path):
        raise BaseException('Not impl')

    def send_audio(self, uid, caption, path):
        raise BaseException('Not impl')

    def send_video(self, uid, caption, path):
        raise BaseException('Not impl')

    def send_buttons(self, uid, text, btns):
        raise BaseException('Not impl')

    def get_user_data(self, uid):
        raise BaseException('Not impl')

    def poll(self):
        self.channel.poll()
        for scenario in self.sessions.values():
            scenario.on_new_message(None)

    def dispatch(self, to, text, sender):
        if isinstance(to, list):
            for item in to:
                self.dispatch(item, text, sender)
        else:
            if to in self.sessions:
                self.sessions[to].on_new_message(text)
            else:
                self.db.append('<message from="%s">%s</message>' % (sender, text), 'sessions/session[@uid="%s"]/messages' % to)

    def activate_session(self, uid, session):
        self.sessions[uid] = session
        self.check_messages(uid)

    def check_messages(self, uid):
        filter = (self.args.messenger, uid)
        with self.db.transaction():
            sessions = self.db.xpath('sessions/session[@uid="%s:%s"]' % filter)
            if sessions:
                messages = sessions[0].messages[0]
                if 'message' in messages:
                    for msg in messages.message:
                        self.sessions[uid].on_remote_message(msg.text(), msg['from'])
                    self.db.replace('sessions/session[@uid="%s:%s"]/messages' % filter, '<messages/>')
            else:
                self.db.append('<session uid="%s:%s"><messages/></session>' % filter, 'sessions')

    def peers(self):
        return self.db.xpath('sessions/session/@uid')


def validate(xml_path):
    from lxml import etree
    xmlschema_doc = etree.parse('scenario.xsd')
    xmlschema = etree.XMLSchema(xmlschema_doc)
    doc = etree.parse(xml_path)
    try:
        xmlschema.assertValid(doc)
        return True
    except etree.DocumentInvalid as error:
        print(error.error_log)
        return False


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--scenario", help="path to xml scenario")
    parser.add_argument("-m", "--messenger", choices=['vk', 'tg', 'icq', 'lt', 'agent', 'viber'], required=True,
                        help="messenger")
    parser.add_argument("-t", "--tocken", help="bot tocken", required=True)
    parser.add_argument("-r", "--record", help="record answers")
    args = parser.parse_args()
    if not validate(args.scenario):
        print("Invalid Scenario File")
        return
    disp = MessageDispatcher(args.scenario, args)
    if args.messenger == 'vk':
        import vk_sai
        disp.set_channel(vk_sai.VKSai(disp, args.tocken))
    elif args.messenger == 'tg':
        import telegram_sai
        disp.set_channel(telegram_sai.TelegramSai(disp, args.tocken))
    elif args.messenger == 'icq':
        import icq_sai
        disp.set_channel(icq_sai.IcqSai(disp, args.tocken))
    elif args.messenger == 'lt':
        disp.set_channel(LocalTest.Chat(disp))
    elif args.messenger == 'agent':
        import agentm_sai
        disp.set_channel(agentm_sai.Agent_sai(disp, args.tocken))
    elif args.messenger == 'viber':
        import viber_sai
        disp.set_channel(viber_sai.ViberSay(disp, args.tocken))
    disp.poll()


def unit_tests():
    res = Ask.regexp(
        '01/01/2000',
        r'/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/gi'
    )
    print(res)


if __name__ == '__main__':
    if sys.argv[1] == 'unittest':
        unit_tests()
    else:
        main()
