import logging.config
from logging import getLogger
from telegram import Bot
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext import CallbackQueryHandler
import channel

TG_API_URL = "https://telegg.ru/orig/bot"
# TG_TOKEN = "1126473938:AAEIwa7mWhVhattx1hH9cExJOpK9mllwlPc"  # t.me/media_chat_bot
TG_TOKEN = "1287903496:AAHrmJ3fkuanO0V-gIGIBVVif3YViBHS_5U"  # t.me/super_scenario_bot


LOGGING = {
        "version":1,
        "handlers":{
            "fileHandler":{
                "class":"logging.FileHandler",
                "formatter":"myFormatter",
                "filename":"history.log"
            }
        },
        "loggers":{
            __name__:{
                "handlers":["fileHandler"],
                "level":"INFO",
            }
        },
        "formatters":{
            "myFormatter":{
                "format":"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            }
        }
    }

logging.config.dictConfig(LOGGING)
logger=getLogger(__name__)


def debug_requests(f):
    #декоратор для отладки событий от телеграма
    def inner(*args, **kwargs):
        try:
            logger.info(f"Обращение в функцию {f.__name__}")
            return f(*args, *kwargs)
        except Exception:
            logger.exception(f"Ошибка в обработчике {f.__name__}")
            raise

    return inner



class TelegramSai(channel.Interface):
    def __init__(self, channel, token):
        self.token=token
        self.channel = channel
        self.last_user = None
        self.bot = Bot(token=self.token, base_url=TG_API_URL)
        self.updater = Updater(bot=self.bot, request_kwargs={'read_timeout': 1000, 'connect_timeout': 1000})
        self.start_handler = CommandHandler("start", self.do_start)
        self.message_handler = MessageHandler(Filters.text, self.do_echo)
        self.buttons_handler = CallbackQueryHandler(callback=self.keyboard_callback_handler, pass_chat_data=True)
        self.updater.dispatcher.add_handler(self.start_handler)
        self.updater.dispatcher.add_handler(self.message_handler)
        self.updater.dispatcher.add_handler(self.buttons_handler)


    def poll(self):
        self.updater.start_polling()
        self.updater.idle()


    def keyboard_callback_handler(self, bot, update, chat_data=None, **kwargs):
        return


    def get_user_data(self, uid):  # FIXME: telegram uses chat id and can have many users in it!
        return self.last_user


    def do_start(self, bot, update):
        self.last_user = update.message.from_user
        self.channel.send_message(update.message.chat_id, '/start', from_user=update.message.from_user, forward=update.message.forward_from)


    def do_echo(self, bot, update):
        self.last_user = update.message.from_user
        self.channel.send_message(update.message.chat_id, update.message.text, from_user=update.message.from_user, forward=update.message.forward_from)


    def send_message(self, uid, text, **kwargs):
        try:
            self.bot.send_message(chat_id=uid, text=text, reply_markup=None)
        except:
            pass        


    def send_photo(self, uid, caption, path_to_photo):
        try:
            self.bot.send_photo(chat_id=uid, photo=open(path_to_photo, 'rb'), caption=caption, reply_markup=None, parse_mode="Markdown")
        except:
            pass


    def send_video(self, uid, caption, path_to_video):
        import telebot
        import pafy
        import os
        youtube_video='youtube.com' in path_to_video
        if youtube_video:
            video = pafy.new(path_to_video)
            best_video = video.getbest()
            video_id = video.videoid # Получаем id видео
            ext = best_video.extension # Получаем расширение видео
            filename = f'{video_id}.{ext}' # Формируем имя файла на основе id + расширения
            best_video.download(filename) # Скачиваем видео и сохраняем с именем videoid+расширение
        else:
            filename=path_to_video
        bot = telebot.TeleBot(self.token)
        try:
            bot.send_video(uid, open(filename,'rb'),'',caption)
        except:
            pass
        if youtube_video:
            try:
                path = os.path.join(os.path.abspath(os.path.dirname(__file__)), filename)
                os.remove(path)
            except:
                pass


    def send_video_alt(self, uid, caption, path_to_video):
        import telebot
        import youtube_dl
        import os
        youtube_video='youtube.com' in path_to_video
        if youtube_video:
            video_name='tmp_video'
            options = {
                'outtmpl': f'{video_name}.mp4',
                'noplaylist': True,
                'continue_dl': True,
            }
            try:
                with youtube_dl.YoutubeDL(options) as ydl:
                    ydl.cache.remove()
                    info_dict = ydl.extract_info(path_to_video, download=False)
                    ydl.prepare_filename(info_dict)
                    try:
                        path = os.path.join(os.path.abspath(os.path.dirname(__file__)), video_name+".mp4")
                        os.remove(path)
                    except:
                        pass
                    ydl.download([path_to_video])
                    filename=video_name+".mp4"
            except Exception:
                pass
        else:
            filename=path_to_video
        bot = telebot.TeleBot(self.token)
        try:
            bot.send_video(uid, open(filename,'rb'),'',caption)
        except Exception:
            pass


    def send_buttons(self, uid, text, btns, inline=False):
        from telegram import ReplyKeyboardMarkup
        keyboard_lines=[]
        for button in btns:
            new_line=[button]
            keyboard_lines.append(new_line)
        try:
            self.bot.send_message(chat_id=uid, text=text, reply_markup=ReplyKeyboardMarkup(keyboard_lines))
        except:
            pass


    def send_audio(self, uid, caption, path):
        import telebot
        try:
            bot = telebot.TeleBot(self.token)
            bot.send_audio(uid,open(path,'rb'), None, 'Audio', caption)
        except:
            pass


if __name__ == '__main__':
    bot=TelegramSai(1,TG_TOKEN)
    #bot.send_video(820811987, "Интересное видео", input("Видеосылка:"))
    #bot.send_audio(820811987,"Музыка", "sound.mp3")
    #bot.send_photo(820811987, "Красивое фото", "photo.jpg")
    #bot.send_buttons(820811987, "Кнопки", ["Кнопка 1","Кнопка 2","Кнопка 3"])
    #bot.send_message(820811987, "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque facere, natus ex, enim doloribus pariatur vero earum. Dicta quidem incidunt rerum eveniet voluptates error necessitatibus nobis rem deserunt. A, maxime.")
    print("Ok!")
