import abc

class Interface(abc.ABC):

    @abc.abstractmethod
    def send_message(self, uid, text, **kwargs):
        pass

    @abc.abstractmethod
    def send_photo(self, uid, caption, path):
        pass

    @abc.abstractmethod
    def send_video(self, uid, caption, path):
        pass

    @abc.abstractmethod
    def send_audio(self, uid, caption, path):
        pass

    @abc.abstractmethod
    def send_buttons(self, uid, text, btns, inline=False):
        pass

    @abc.abstractmethod
    def poll(self):
        pass

    @abc.abstractmethod
    def get_user_data(self, uid):
        pass
