import channel
import sys
import tkinter
from tkinter import ttk

#  https://stackoverflow.com/questions/49307497/python-tkinter-treeview-add-an-image-as-a-column-value


class Chat(channel.Interface):
    def __init__(self, channel):
        self.channel = channel
        self.tk = tkinter.Tk()

        self.tk.title('Chat Bot')
        self.tk.geometry('400x500')
        self.tk.columnconfigure(0, weight=1)
        self.tk.rowconfigure(0, weight=1)

        self.chat = tkinter.Text(self.tk, borderwidth=1, relief='sunken', state=tkinter.DISABLED)
        self.chat.grid(row=0, sticky='nswe')
        self.scroll = ttk.Scrollbar(self.tk, command=self.chat.yview)
        self.scroll.grid(row=0, column=1, sticky='nsw')
        self.chat['yscrollcommand'] = self.scroll.set

        self.input = tkinter.Text(self.tk, borderwidth=1, relief='sunken', height=1, wrap='none')
        self.input.grid(row=1, sticky='swe', columnspan=2)
        self.input.bind("<Return>", lambda evt: self.on_enter(evt))
        self.input.bind("<Key>", lambda evt: self.input.focus_set())

        self.buttons = tkinter.Frame(self.tk, height=20)
        self.buttons.grid(row=2, sticky='we', columnspan=2)

        self.activate()

    def activate(self):
        win = self.tk
        win.update_idletasks()
        width = win.winfo_width()
        frm_width = win.winfo_rootx() - win.winfo_x()
        win_width = width + 2 * frm_width
        height = win.winfo_height()
        titlebar_height = win.winfo_rooty() - win.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = win.winfo_screenwidth() // 2 - win_width // 2
        y = win.winfo_screenheight() // 2 - win_height // 2
        win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        win.deiconify()
        self.tk.lift()
        self.tk.call('wm', 'attributes', '.', '-topmost', True)
        self.tk.after_idle(self.tk.call, 'wm', 'attributes', '.', '-topmost', False)
        self.tk.update()
        self.input.focus_set()

    def on_enter(self, evt):
        txt = self.input.get("1.0", tkinter.END).strip()
        self.answer(txt)
        self.tk.after_idle(lambda: self.input.delete('1.0', tkinter.END))
        self.tk.update()

    def answer(self, txt):
        self.channel.send_message(1, txt)
        self.chat.configure(state='normal')
        self.chat.insert(tkinter.END, "\nYou: >>\t" + txt)
        self.chat.configure(state='disabled')
        self.tk.update()

    def send_message(self, uid, text, **kwargs):  # incoming from bot
        self.chat.configure(state='normal')
        self.chat.insert(tkinter.END, "\nBot: >>\t" + text)
        self.chat.see(tkinter.END)
        self.chat.configure(state='disabled')
        self.tk.update()

    def send_photo(self, uid, caption, path):
        self.send_message(uid, caption + ':' + path)

    def send_video(self, uid, caption, path):
        raise BaseException('Not impl')

    def send_buttons(self, uid, text, btns):
        self.send_message(uid, text)
        for child in self.buttons.winfo_children():
            child.destroy()
        for btn in btns:
            tkinter.Button(self.buttons, text=btn, command=lambda val=btn: self.on_select(val)).pack(side="left")
        self.tk.update()

    def get_user_data(self, uid):
        return {'first_name': 'Local', 'last_name': 'Local'}

    def on_select(self, btn):
        self.answer(btn)

    def poll(self):
        self.tk.mainloop()
