import os
import sys
from lxml import etree

if sys.platform == 'win32':
    import msvcrt

    def lock_file(f):
        msvcrt.locking(f.fileno(), msvcrt.LK_RLCK, os.path.getsize(os.path.realpath(f.name)))

    def unlock_file(f):
        msvcrt.locking(f.fileno(), msvcrt.LK_UNLCK, os.path.getsize(os.path.realpath(f.name)))

else:
    # Posix based file locking (Linux, Ubuntu, MacOS, etc.)
    import fcntl

    def lock_file(f):
        fcntl.lockf(f, fcntl.LOCK_EX)

    def unlock_file(f):
        fcntl.lockf(f, fcntl.LOCK_UN)


# Class for ensuring that all file operations are atomic, treat
# initialization like a standard call to 'open' that happens to be atomic.
# This file opener *must* be used in a "with" block.
class AtomicOpen:
    # Open the file with arguments provided by user. Then acquire
    # a lock on that file object (WARNING: Advisory locking).
    def __init__(self, path, *args, **kwargs):
        # Open the file and acquire a lock on the file before operating
        self.file = open(path, *args, **kwargs)
        # Lock the opened file
        lock_file(self.file)

    # Return the opened file object (knowing a lock has been obtained).
    def __enter__(self, *args, **kwargs):
        return self.file

    # Unlock the file and close the file object.
    def __exit__(self, exc_type=None, exc_value=None, traceback=None):
        # Flush to make sure all buffered contents are written to file.
        self.file.flush()
        os.fsync(self.file.fileno())
        # Release the lock on the file.
        unlock_file(self.file)
        self.file.close()
        # Handle exceptions that may have come up during execution, by
        # default any exceptions are raised to the user.
        if exc_type is not None:
            return False
        return True


class Persistence:
    def __init__(self, path):
        self.path = path
        with AtomicOpen(self.path + '.lock', 'w'):
            if not os.path.exists(self.path):
                self.__write(etree.fromstring('<db/>').getroottree())

    def __read(self):
        return etree.parse(self.path)

    def __write(self, dom):
        return dom.write(self.path, pretty_print=True, xml_declaration=True, encoding="utf-8")

    def __create_recusively(self, dom, parts):
        all_but_last = parts[:-1]
        last = parts[-1]
        assert(not last.endswith(']'))  # filter will work only for pre-created elements
        xpath = '/'.join(all_but_last)
        parents = dom.xpath(xpath) if xpath else [dom]
        if not parents:
            parents = [self.__create_recusively(dom, all_but_last)]
        res = []
        for parent in parents:
            res.append(etree.fromstring('<%s/>' % last))
            parent.append(res[-1])
        return res

    def append(self, xml, xpath=None):
        child = etree.fromstring(xml)
        with self.transaction():
            dom = self.__read()
            if xpath:
                parents = dom.xpath(xpath) or self.__create_recusively(dom.getroot(), xpath.split('/'))
            else:
                parents = [dom.getroot()]
            for parent in parents:
                parent.insert(0, child)
            self.__write(dom)

    def xpath(self, xpath):  # returns all matching
        with self.transaction():
            dom = self.__read()
        res = dom.getroot().xpath(xpath)
        return [(el if isinstance(el, str) else Accessor(el)) for el in res]

    def replace(self, xpath, xml):
        child = etree.fromstring(str(xml)) if xml else None
        with self.transaction():
            dom = self.__read()
            parents = set()
            for item in dom.xpath(xpath):
                parent = item.getparent()
                parents.add(parent)
                parent.remove(item)
            if child is not None:
                for parent in parents:
                    parent.insert(0, child)
            self.__write(dom)

    def delete(self, xpath):
        self.replace(xpath, None)

    def transaction(self):
        return AtomicOpen(self.path + '.lock', 'w')

class Accessor(object):
    def __init__(self, dom):
        super().__setattr__('__dom', dom)

    def dom(self):
        return super().__getattribute__('__dom')

    def __getattr__(self, name):
        if name.startswith('__'):
            return super().__getattribute__(name)
        else:
            children = self.dom().findall(name)
            if not children:
                raise KeyError('"%s" is not found here: %s' % (name, self.__repr__()))
            return [Accessor(child) for child in children]

    def __contains__(self, name):
        return self.dom().findall(name)

    def __setattr__(self, key, value):
        el = etree.fromstring(value)
        assert(el.tag == key)
        self.dom().append(el)

    def __getitem__(self, key):
        return self.dom().attrib[key]

    def __setitem__(self, key, value):
        self.dom().attrib[key] = str(value)

    def __repr__(self):
        return etree.tostring(self.dom(), pretty_print=True).decode().strip()

    def text(self):
        return self.dom().text

def main():
    acc = Accessor(etree.fromstring('<a><node/><node/></a>'))
    print(acc.__dom)
    acc['attr'] = 1
    print(acc['attr'])
    acc.node[1]
    acc.node[0].child = '<child/>'

    ps = Persistence('/tmp/ps.xml')
    ps.append('<a/>')
    d = ps.xpath('//a')
    print(d)


if __name__ == '__main__':
    main()
elif 'api' in globals():  # loaded from bot
    setattr(api, 'db', Persistence('db.xml'))  # api is in globals when loaded from bot
